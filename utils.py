import os
import sys
import xlsxwriter
import cx_Oracle
import time
import configparser
import json

#json1_data = json.loads(json1_str)

class DBCollector:
    def __init__(self,section,conf_file):
        self.cfg = {}
        section = section
        self.config = configparser.ConfigParser()
        self.flag = True
        self.binds = {}
        self.config_file = conf_file
        self.loadConfig(section)
        self.loadBinds()

    def loadConfig(self,section):
      if os.path.exists(self.config_file)==False:
          raise Exception("%s file does not exist.\n" % self.config_file)

      config = self.config
      config.read(self.config_file)

      self.db_sid     = config.get(section,'DB_SID')
      self.host       = config.get(section,'Host')
      self.port       = config.get(section,'Port')
      self.user       = config.get(section,'User')
      self.password   = config.get(section,'Password')
      self.output_dir = self.config.get(section,'Output_Dir')

    def loadBinds(self):
      section = "query_variables"
      options = self.config.options(section)
      for option in options:
        try:
          self.binds[option] = self.config.get(section, option)
          print("%s of %s => %s" % (option,section, self.binds[option]))
          if self.binds[option] == -1:
            DebugPrint("skip: %s of %s" % (option,section))
        except configparser.Error as e:
          print("exception on %s of %s!" % (option,section))
          print("Configuration Error:", e, file=sys.stdout)
          sys.exit(1)
        except:
          print("exception on %s of %s!" % (option,section))
          self.binds[option] = None

    def getSectionSQLs(self,section):

      config = self.config

      queries = {}
      options = config.options(section)
      for option in options:
        try:
          query_str = config.get(section, option)
          if query_str == -1:
            DebugPrint("skip: %s of %s" % (option,section))

          #print(query_str)
          for bind in self.binds.keys():
            query_str = query_str.replace("@@%s@@" % bind, self.binds.get(bind))

          #print(query_str)

          queries[option] = query_str
        except configparser.Error as e:
          print("exception on %s of %s!" % (option,section))
          print("Configuration Error:", e, file=sys.stdout)
          sys.exit(1)
        except:
          print("exception on %s of %s!" % (option,section))
          queries[option] = None
      return queries

    def getSectionData(self,section):

      config = self.config

      info = {}
      options = config.options(section)
      for option in options:
        try:
          info[option] = config.get(section, option)
          if info[option] == -1:
            DebugPrint("skip: %s of %s" % (option,section))
        except configparser.Error as e:
          print("exception on %s of %s!" % (option,section))
          print("Configuration Error:", e, file=sys.stdout)
          sys.exit(1)
        except:
          print("exception on %s of %s!" % (option,section))
          info[option] = None
      return info

#def logging(ws_log, row, work_name, start_time, query_time, end_time, query_result):
#  row += 1
#  ws_log.write(row, 0, work_name   , border1      )
#  ws_log.write(row, 1, start_time  , f_dt_datetime)
#  ws_log.write(row, 2, query_time  , f_dt_datetime)
#  ws_log.write(row, 3, end_time    , f_dt_datetime)
#  ws_log.write(row, 4, query_result, border1      )
